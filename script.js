'use strict';
function app() {
    var i, allCharacters, background, bloodyBackground, playButton, gunMan1, curtainGunMan,fireMessage,
        winMessage, failMessage, header, footer, createdBackground, flag, lives, moneyCounterBlock,
        livesCounterBlock, money;
    lives = 3;
    money = 0;


    allCharacters = {
        gunManMovesIn: [
            'animation: walkGunMan1 0.4s steps(3) 12, moveIn 4700ms steps(200) 1; background: url("GunMan_1.png");' +
            ' margin: auto; position: relative; width: 95px; height: 192px;',
            'animation: walkGunMan2 0.4s steps(2) 12, moveIn 4700ms steps(200) 1; background: url("GunMan_2.png");' +
            ' margin: auto; position: relative; width: 99px; height: 216px;',
            'animation: walkGunMan3 0.4s steps(2) 12, moveIn 4700ms steps(200) 1; background: url("GunMan_3.png");' +
            ' margin: auto; position: relative; width: 81px; height: 242px;',
            'animation: walkGunMan4 0.4s steps(2) 12, moveIn 4700ms steps(200) 1; background: url("GunMan_4.png");' +
            ' margin: auto; position: relative; width: 99px; height: 198px;',
            'animation: walkGunMan5 0.4s steps(2) 12, moveIn 4700ms steps(200) 1; background: url("GunMan_5.png");' +
            ' margin: auto; position: relative; width: 98px; height: 213px;'
        ],
        gunManPreStand: [
            'background: url("GunMan_1.png"); background-position: -296px; margin: auto; position: relative;' +
            ' width: 100px; height: 192px;',
            'background: url("GunMan_2.png"); background-position: 0px; margin: auto; position: relative; ' +
            'width: 96px; height: 216px;',
            'background: url("GunMan_3.png"); background-position: 0px; margin: auto; position: relative; ' +
            'width: 81px; height: 242px;',
            'background: url("GunMan_4.png"); background-position: 2px; margin: auto; position: relative; ' +
            'width: 99px; height: 198px;',
            'background: url("GunMan_5.png"); background-position: 0px; margin: auto; position: relative; ' +
            'width: 98px; height: 213px;'
        ],
        gunManStopped: [
            'background: url("GunMan_1.png"); background-position: -297px; margin: auto; position: relative; ' +
            'width: 98px; height: 192px;',
            'background: url("GunMan_2.png"); background-position: 0px; margin: auto; position: relative; ' +
            'width: 96px; height: 216px;',
            'background: url("GunMan_3.png"); background-position: 0px; margin: auto; position: relative; ' +
            'width: 81px; height: 242px;',
            'background: url("GunMan_4.png"); background-position: 2px; margin: auto; position: relative; ' +
            'width: 99px; height: 198px;',
            'background: url("GunMan_5.png"); background-position: 0px; margin: auto; position: relative; ' +
            'width: 98px; height: 213px;'
        ],
        shootingGunMan:[
            'background: url("GunMan_1.png"); background-position: -603px; margin: auto; position: relative; ' +
            'width: 98px; height: 192px;',
            'background: url("GunMan_2.png"); background-position: -301px; margin: auto; position: relative; ' +
            'width: 93px; height: 216px;',
            'background: url("GunMan_3.png"); background-position: -250px; margin: auto; position: relative; ' +
            'width: 88px; height: 242px;',
            'background: url("GunMan_4.png"); background-position: -304px; margin: auto; position: relative; ' +
            'width: 99px; height: 198px;',
            'background: url("GunMan_5.png"); background-position: -300px; margin: auto; position: relative; ' +
            'width: 92px; height: 213px;'
        ],
        fallingGunMan: [
            'animation: gunMan1Falling 0.3s steps(1) 1; background: url("GunMan_1.png"); margin: auto; ' +
            'position: relative; width: 98px; height: 192px;',
            'animation: gunMan2Falling 0.3s steps(2) 1; background: url("GunMan_2.png"); margin: auto; ' +
            'position: relative; width: 86px; height: 216px;',
            'background: url("GunMan_3.png"); background-position: -614px; margin: auto; position: relative; ' +
            'width: 96px; height: 242px;',
            'background: url("GunMan_4.png"); background-position: -701px; margin: auto; position: relative; ' +
            'width: 96px; height: 198px;',
            'animation: gunMan5Falling 0.3s steps(1) 1; background: url("GunMan_5.png"); margin: auto;' +
            ' position: relative; width: 102px; height: 213px;'
        ],
        gunManPositionAfterFall: [
            'background: url("GunMan_1.png"); background-position: -1115px; margin: auto; position: relative; ' +
            'width: 100px; height: 192px;',
            'background: url("GunMan_2.png"); background-position: -713px; margin: auto; position: relative; ' +
            'width: 96px; height: 216px;',
            'background: url("GunMan_3.png"); background-position: -614px; margin: auto; position: relative; ' +
            'width: 96px; height: 242px;',
            'background: url("GunMan_4.png"); background-position: -701px; margin: auto; position: relative; ' +
            'width: 96px; height: 198px;',
            'background: url("GunMan_5.png"); background-position: -777px; margin: auto; position: relative; ' +
            'width: 102x; height: 213px;'
        ],
        laughingGunMan: [
            'animation: laughingGunMan1 1s steps(3) 1; background: url("GunMan_1.png"); margin: auto; ' +
            'position: relative; width: 98px; height: 192px;',
            'animation: laughingGunMan2 1s steps(3) 1; background: url("GunMan_2.png"); margin: auto; ' +
            'position: relative; width: 76px; height: 216px;',
            'animation: laughingGunMan3 1s steps(3) 1; background: url("GunMan_3.png"); margin: auto; ' +
            'position: relative; width: 80px; height: 242px;',
            'animation: laughingGunMan4 1s steps(3) 1; background: url("GunMan_4.png"); margin: auto; ' +
            'position: relative; width: 99px; height: 198px;',
            'animation: laughingGunMan5 1s steps(3) 1; background: url("GunMan_5.png"); margin: auto; ' +
            'position: relative; width: 93px; height: 213px;'
        ],
        gunManWalkingAway: [
            'animation: walkGunMan1 0.4s steps(3) 12, moveOut 4900ms steps(200) 1; background: url("GunMan_1.png"); ' +
            'margin: auto; position: relative; width: 95px; height: 192px; transform: scale(-1,1);',
            'animation: walkGunMan1 0.4s steps(3) 12, moveOut1 4900ms steps(200) 1; background: url("GunMan_2.png"); ' +
            'margin: auto; position: relative; width: 99px; height: 216px; transform: scale(-1,1);',
            'animation: walkGunMan3 0.4s steps(2) 12, moveOut 4900ms steps(200) 1; background: url("GunMan_3.png"); ' +
            'margin: auto; position: relative; width: 81px; height: 242px; transform: scale(-1,1);',
            'animation: walkGunMan4 0.4s steps(2) 12, moveOut1 4900ms steps(200) 1; background: url("GunMan_4.png"); ' +
            'margin: auto; position: relative; width: 99px; height: 198px; transform: scale(-1,1);',
            'animation: walkGunMan5 0.4s steps(2) 12, moveOut 4900ms steps(200) 1; background: url("GunMan_5.png"); ' +
            'margin: auto; position: relative; width: 98px; height: 213px;'
        ]
    };
    background = document.createElement('div');
    bloodyBackground = document.createElement('div');
    playButton = document.createElement('p');
    gunMan1 = document.createElement('div');
    curtainGunMan = document.createElement('div');
    fireMessage = document.createElement('p');
    winMessage = document.createElement('p');
    moneyCounterBlock = document.createElement('p');
    livesCounterBlock = document.createElement('p');
    failMessage = document.createElement('p');
    header = document.createElement('div');
    footer = document.createElement('div');
    createdBackground = document.body.appendChild(background);

    playButton.innerHTML = 'PLAY!';
    winMessage.innerHTML = 'YOU WON!';
    failMessage.innerHTML = 'YOU LOST!!';
    moneyCounterBlock.innerHTML = 'money: 0$';
    livesCounterBlock.innerHTML = 'lives: ' + lives;

    playButton.style.cssText = ('font-family: Impact; border: 4px solid black;border-radius: 10px;width: 100px;' +
    'height: 60px;background-color: white;line-height: 60px;' +
    'text-align: center;margin: auto; font-size: 40px');
    moneyCounterBlock.style.cssText = ('font-family: Impact; background-color: black; color: green; line-height: 60px;' +
    'text-align: center;margin: auto; font-size: 20px');
    livesCounterBlock.style.cssText = ('font-family: Impact; background-color: black; color: green; line-height: 60px;' +
    'text-align: center;margin: auto; font-size: 20px');

    background.style.cssText = ('display: flex; margin: auto; background: url("bg.png");width: 792px;height: 402px;');
    header.style.cssText = ('margin: auto; background: black; width: 792px; height: 70px;');
    footer.style.cssText = ('display: flex; flex-direction: row; justify-content: space-between; margin: auto;' +
    ' background: black; width: 792px; height: 70px;');
    footer.appendChild(moneyCounterBlock);
    footer.appendChild(livesCounterBlock);
    createdBackground.appendChild(playButton);
    document.body.insertBefore(header, createdBackground);
    document.body.appendChild(footer);
    document.body.style.backgroundColor = 'black';



    function makeRandomTimeOutBeforeShhot() {
        var rand = 200 - 0.5 + Math.random() * (1000 + 1)
        rand = Math.round(rand);
        return rand;
    }
    function makeRandomCharacter() {
        var rand = 0.3 + Math.random() * (4)
        rand = Math.round(rand);

        return rand;
    }
    function makeRandomTimeOut() {
        var rand = 500 - 0.5 + Math.random() * (2000 + 1)
        rand = Math.round(rand);
        return rand;
    }
    function win(){
        gunMan1.appendChild(curtainGunMan);
        curtainGunMan.style.display = '';
        document.getElementById('shot-fall').play();
        gunMan1.style.cssText = allCharacters.fallingGunMan[i];
        gunMan1.addEventListener("animationend", function () {
            gunMan1.style.cssText = allCharacters.gunManPositionAfterFall[i];
        });
        winMessage.style.cssText = ('font-family: Impact; border: 4px solid black;border-radius: 10px;' +
        ' width: 200px; height: 60px; background-color: white;line-height: 60px;' +
        ' position: absolute; text-align: center; font-size: 40px;' +
        ' margin-left: 100px; margin-top: 90px;');
        header.appendChild(winMessage);
        document.getElementById('win').play();
        document.getElementById('win').onended = function () {
            winMessage.style.display = 'none';
            money += 500;
            moneyCounterBlock.innerHTML = 'money: ' + money + '$';
            start();
        };
    }
    function dead(){
        document.getElementById('shot').play();
        document.getElementById('death').play();

        failMessage.style.cssText = ('font-family: Impact; border: 4px solid black; border-radius: 10px;' +
        ' width: 200px; height: 60px; background-color: white; line-height: 60px;' +
        ' text-align: center; font-size: 40px; margin-top: 80px;' +
        ' position: absolute; margin-left: 450px;');

        document.getElementById('shot').onplay = function (){
            gunMan1.style.cssText = (allCharacters.laughingGunMan[i]);
        };
        document.getElementById('shot').onended = function () {
            gunMan1.style.cssText = (allCharacters.gunManWalkingAway[i]);
        };

        background.appendChild(bloodyBackground);

        header.appendChild(failMessage);

        bloodyBackground.style.cssText = ('display: flex; margin: auto; position: absolute; width: 792px;' +
        ' height: 402px; background: red; opacity: 0.3; margin-left: -792px;');


        document.getElementById('death').addEventListener('ended',deathMusicHandler );
        function deathMusicHandler() {
            fireMessage.style.display = 'none';
            failMessage.style.display = 'none';
            bloodyBackground.style.display = 'none';
            start();
        }

        lives--;
        livesCounterBlock.innerHTML = 'lives: ' + lives;
        if(lives <= 0){
            document.getElementById('death').addEventListener('ended', setTimeout(function(){
                location.reload();
            }, 6800) );
        }

    }
    function winOrDead(){
        if(flag == 1){
            win();
        }else{
            dead();
        }
    }
    function start(){
        flag = 0;
        i = makeRandomCharacter();
        playButton.style.display = "none";
        document.getElementById('intro').play();
        createdBackground.appendChild(gunMan1);
        gunMan1.style.cssText = (allCharacters.gunManMovesIn[i]);
        gunMan1.appendChild(curtainGunMan);
        gunMan1.addEventListener('animationend', function () {
            gunMan1.style.cssText = allCharacters.gunManPreStand[i];
        });
        curtainGunMan.style.cssText = ('background: grey; width: 102px; height: 242px; opacity: 0;');
        document.getElementById('intro').onended = function () {
            gunMan1.style.cssText = (allCharacters.gunManStopped[i]);
            document.getElementById('wait').play();
            setTimeout(function(){
                fireMessage.innerHTML = 'FIRE!';
                fireMessage.style.cssText = ('font-family: Impact; border: 4px solid black; border-radius: 10px; width: 100px;' +
                'height: 60px; background-color: white; line-height: 60px;' +
                'text-align: center; font-size: 40px; margin-top: 80px; position:absolute; margin-left: 450px;');

                header.appendChild(fireMessage);
                gunMan1.style.cssText = allCharacters.shootingGunMan[i];
                document.getElementById('fire').play();
                document.getElementById('fire').onplay = setTimeout(winOrDead, makeRandomTimeOutBeforeShhot());
                document.getElementById('fire').onended = function () {
                    fireMessage.style.display = 'none';
                };
                curtainGunMan.style.display = "none";
                gunMan1.addEventListener('click', function(){
                    flag = 1;
                    curtainGunMan.style.display = '';
                });
                document.getElementById('wait').onended = function () {
                    fireMessage.style.display = 'none';
                };
            }, makeRandomTimeOut());
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    background.onclick = function () {
        var target = event.target;

        if (target == playButton) {
            start();
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
app();
//location.reload();